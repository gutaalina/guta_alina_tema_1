// Return true if item exists in array, otherwise false
function isItemInArray(item, array) {
    // We assume the array received is really an array, we don't test it any more
    return array.includes(item);
}

// Count the number of elements of the first array that are not in the second
function getCountOfDifferences(array1, array2) {
    let count = 0;
    for (index = 0; index < array1.length; ++index)
        if (!isItemInArray(array1[index], array2))
            count++;

    return count;
}

// Remove duplicates from an array.
// From here:
// https://stackoverflow.com/questions/1960473/get-all-unique-values-in-a-javascript-array-remove-duplicates
function removeDuplicates(array) {
    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

    return array.filter(onlyUnique);
}

function distance(array1, array2) {
    // Check if both arrays are defined
    if (!(array1 && array2))
        throw new Error('InvalidType');

    // Check if the arguments are arrays
    if (!(Array.isArray(array1) && Array.isArray(array2)))
        throw new Error('InvalidType');

    // Check if the arrays are both empty
    if (array1.length == 0 && array2.length == 0)
        return 0;

    // If we are here, this means the arrays exists, are really arrays and are not empty
    //
    // Get the arrays with the duplicates removed
    let array1_u = removeDuplicates(array1);
    let array2_u = removeDuplicates(array2);
    //console.log(array1_u);
    //console.log(array2_u);

    // Count the number of elements of the first array that are not in the second
    let count1 = getCountOfDifferences(array1_u, array2_u);
    let count2 = getCountOfDifferences(array2_u, array1_u);

    return count1 + count2;
}

module.exports.distance = distance